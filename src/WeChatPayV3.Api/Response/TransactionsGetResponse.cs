﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeChatPayV3.Model.Base;

namespace WeChatPayV3.Response
{
    public class TransactionsGetResponse : WechatPayBaseResponse
    {
        /// <summary>
        /// 应用ID    string[1,32]    是    直连商户申请的公众号或移动应用appid。                示例值：wxd678efh567hg6787
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 直连商户号    string[1,32]    是    直连商户的商户号，由微信支付生成并下发。 示例值：1230000109
        /// </summary>
        public string mchid { get; set; }

        /// <summary>
        /// 商户订单号    string[6,32]    是    商户系统内部订单号，
        /// 只能是数字、大小写字母_-*且在同一个商户号下唯一，详见【商户订单号】。        示例值：1217752501201407033233368018
        /// </summary>
        public string out_trade_no { get; set; }

        /// <summary>
        /// 微信支付订单号 string[1, 32] 否   微信支付系统生成的订单号。
        /// 示例值：1217752501201407033233368018
        /// </summary>
        public string transaction_id { get; set; }
        /// <summary>
        /// 交易类型 string[1, 16] 否   交易类型，枚举值：
        /// JSAPI：公众号支付
        /// NATIVE：扫码支付
        /// APP：APP支付
        /// MICROPAY：付款码支付
        /// MWEB：H5支付
        /// FACEPAY：刷脸支付
        /// 示例值：MICROPAY
        /// </summary>
        public string trade_type { get; set; }
        /// <summary>
        /// 交易状态 string[1, 32] 是   交易状态，枚举值：
        /// SUCCESS：支付成功
        /// REFUND：转入退款
        /// NOTPAY：未支付
        /// CLOSED：已关闭
        /// REVOKED：已撤销（付款码支付）
        ///  USERPAYING：用户支付中（付款码支付）
        ///  PAYERROR：支付失败(其他原因，如银行返回失败)
        ///   ACCEPT：已接收，等待扣款
        ///   示例值：SUCCESS
        /// </summary>
        public string trade_state { get; set; }
        /// <summary>
        /// 交易状态描述 string[1, 256] 是   交易状态描述
        /// 示例值：支付成功
        /// </summary>
        public string trade_state_desc { get; set; }
        /// <summary>
        /// 付款银行 string[1, 16] 否   银行类型，采用字符串类型的银行标识。银行标识请参考《银行类型对照表》 示例值：CMC
        /// </summary>
        public string bank_type { get; set; }

        /// <summary>
        /// 附加数据 string[1, 128] 否   附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用
        /// 示例值：自定义数据
        /// </summary>
        public string attach { get; set; }

        /// <summary>
        /// 支付完成时间 string[1, 64] 否   支付完成时间，
        /// 遵循rfc3339标准格式，格式为YYYY-MM-DDTHH:mm:ss+TIMEZONE，
        /// YYYY-MM-DD表示年月日，
        /// T出现在字符串中，表示time元素的开头，HH:mm:ss表示时分秒，
        /// TIMEZONE表示时区（+08:00表示东八区时间，领先UTC 8小时，即北京时间）。
        /// 例如：2015-05-20T13:29:35+08:00表示，北京时间2015年5月20日 13点29分35秒。
        /// 示例值：2018-06-08T10:34:56+08:00
        /// </summary>
        public string success_time { get; set; }
    }
}
