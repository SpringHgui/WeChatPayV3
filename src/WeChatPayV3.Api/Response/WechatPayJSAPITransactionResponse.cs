﻿
using Newtonsoft.Json;
using WeChatPayV3.Interface;
using WeChatPayV3.Model.Base;

namespace WeChatPayV3.Response
{
    public class WechatPayJSAPITransactionResponse : WechatPayBaseResponse, IWechatPaySDK
    {
        /// <summary>
        /// 预支付订单ID
        /// </summary>
        [JsonProperty(PropertyName = "prepay_id")]
        public string PrepayId { get; set; }

        string IWechatPaySDK.Package
        {
            get { return $"prepay_id={PrepayId}"; }

        }
    }
}
