﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeChatPayV3.Request;
using WeChatPayV3.Response;

namespace WeChatPayV3.Api
{
    public class WeChatPayApi
    {
        WechatOptions clientOption;
        WechatPayClient client;

        public WeChatPayApi(string appId, string merchantId, string certificatePath, string apiSecret)
        {
            client = new WechatPayClient();
            clientOption = new WechatOptions(appId, merchantId, certificatePath, apiSecret);
        }

        /// <summary>
        /// 订单查询
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<TransactionsGetResponse> TransactionsGet(TransactionsGetRequest request)
        {
            return await client.ExecuteRequestAsync(request, clientOption);
        }
    }
}
