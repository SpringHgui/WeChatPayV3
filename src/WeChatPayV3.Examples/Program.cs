﻿using System;
using System.Threading.Tasks;
using WeChatPayV3.Api;
using WeChatPayV3.Request;
using WeChatPayV3.Response;

namespace WeChatPayV3.Examples
{
    class Program
    {
        static void Main(string[] args)
        {
            var appid = "xxxxxx";
            var merch = "xxxxxx";
           
            var apiSecret = "xxxxxx";                                                       // apiV3密钥
            var cerpath = AppDomain.CurrentDomain.BaseDirectory + "\\apiclient_cert.p12";   // api证书

            // 以订单查询接口为例
            TransactionsGetResponse res1 = 方式1Async(apiSecret, appid, merch, cerpath).Result;
            TransactionsGetResponse res2 = 方式2Async(apiSecret, appid, merch, cerpath).Result;

            Console.ReadLine();
        }

        private static async Task<TransactionsGetResponse> 方式1Async(string apiSecret, string appid, string merch, string cerpath)
        {
            // 通过 WeChatPayV3.Api 封装好的接口调用
            var api = new WeChatPayApi(appid, merch, cerpath, apiSecret);
            return await api.TransactionsGet(new TransactionsGetRequest("122222", "1609892697"));
        }

        private static async Task<TransactionsGetResponse> 方式2Async(string apiSecret, string appid, string merch, string cerpath)
        {
            // 直接通过 WeChatPayV3.SDK 基础sdk调用
            // 适用于本库未实现的api，可自行创建请求实体类和响应类
            var client = new WechatPayClient();
            return await client.ExecuteRequestAsync(
                new TransactionsGetRequest("122222", "1609892697"), new WechatOptions(appid, merch, cerpath, apiSecret));
        }
    }
}
