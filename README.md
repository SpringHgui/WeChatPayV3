# WeChatPayV3
轻量高性能 微信支付apiV3库

有帮助的朋友不要吝啬你们的小星星哦~
***

# 项目架构
| 项目 | nuget | 描述 |
| :----- | :---- | :---- |
| WeChatPayV3 | [![Nuget](https://img.shields.io/nuget/v/WeChatPayV3)](https://www.nuget.org/packages/WeChatPayV3/) | 核心库，仅包含微信支付接口调用基础sdk，可以支持所有接口（需要自己创建对应接口的实体类） |
| WeChatPayV3.Api | [![Nuget](https://img.shields.io/nuget/v/WeChatPayV3.Api)](https://www.nuget.org/packages/WeChatPayV3.Api/) | 依赖核心库，增加部分接口的实现，未实现的接口仍需需要基础库来实现 |
| WeChatPayV3.Examples |  | 调用示例demo |

# 快速上手
1. 安装nuget包 `WeChatPayV3`
2. 调用  以订单查询为例
```c#
  // 实例化客户端
  var client = new WechatPayClient();
  // 根据微信官方文档编写TransactionsGetRequest请求实体类
  var request = new TransactionsGetRequest("订单号", "商户号");
  // 接口调用参数
  var options = new WechatOptions(appid, merch, cerpath, apiSecret);
  // 发起调用请求
  TransactionsGetResponse response = await client.ExecuteRequestAsync(request, options);
```
3. 如果安装了nuget包 `WeChatPayV3.Api`，可简化为
```c#
  var api = new WeChatPayApi(appid, merch, cerpath, apiSecret);
  TransactionsGetResponse response = await api.TransactionsGet(new TransactionsGetRequest("订单号", "商户号"));
```
4. 步骤2和3的区别主要是
2中的`TransactionsGetRequest`类和`TransactionsGetResponse`需要使用者自己创建类文件
3中因为`WeChatPayV3.Api`库已经实现了这两个类，并做了接口的封装，不需要自己创建额外的类文件


# WeChatPayV3.Api 接口一览

WeChatPayV3.Api 中未实现的接口不代表本库不支持调用，只是需要使用者自己去创建为实现类的类文件通过调用`WeChatPayV3`基础库中的`ExecuteRequestAsync`方法即可

## 已的接口如下（未实现的接口仅需简单增加两个实体类）
|  接口名称 | 方法  | 状态 |
|  ----  | ----  | ---- |
|  订单查询  | TransactionsGet | ✔ |

作者时间精力有限，欢迎将代码合并到本仓库。

# 有什么问题请提交issue讨论
