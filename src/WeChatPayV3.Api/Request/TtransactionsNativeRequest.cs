﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeChatPayV3.Interface;
using WeChatPayV3.Response;

namespace WeChatPayV3.Request
{
    public class TtransactionsNativeRequest : IWechatPayRequestSDK<TtransactionsNativeResponse>
    {
        public string RequestMethod => "POST";

        public string RequestUrl => "https://api.mch.weixin.qq.com/v3/pay/transactions/native";

        public bool ValidateResponse => false;
    }
}
