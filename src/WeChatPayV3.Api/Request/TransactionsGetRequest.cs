﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeChatPayV3.Interface;
using WeChatPayV3.Response;

namespace WeChatPayV3.Request
{
    public class TransactionsGetRequest : IWechatPayRequestSDK<TransactionsGetResponse>
    {
        readonly string transaction_id;
        readonly string mchid;

        public TransactionsGetRequest(string transactionId, string mchid)
        {
            transaction_id = transactionId;
            this.mchid = mchid;
        }

        public string RequestMethod => "GET";

        public string RequestUrl => $"https://api.mch.weixin.qq.com/v3/pay/transactions/id/{transaction_id}?mchid={mchid}";

        public bool ValidateResponse => false;
    }
}
